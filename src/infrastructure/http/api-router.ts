import {Router, Application} from 'express';
import { registerIssuesController } from '../../domain/issue/controller';


const router = Router();

export const registerApiRouter = (app: Application): void => {
    registerIssuesController(router);
    app.use('/api', router);
};