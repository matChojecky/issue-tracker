import { writable, derived } from "svelte/store";
import type { Issue } from "../../../domain/issue/model";

function createIssuesStore() {
  const { subscribe, set, update } = writable<{
    status: string;
    data: null | Issue[];
    error: any;
  }>({ status: "not_asked", data: null, error: null });

  return {
    subscribe,
    changeStatus(status) {
      update((state) => (state.status = status));
    },
    set(status, data: Issue[]) {
      update((state) => ({
        ...state,
        status,
        data,
      }));
    },
    addIssue(issue: Issue) {
        update(state => {
            state?.data.push(issue);
            return state;
        })
    },
    removeIssue(id: string) {
        update(state => {
            const newlist = state?.data.filter(x => x.id !== id);
            return{
                ...state,
                data: newlist
            }
        })
    },
    updateIssue(id: string, newEntry: Issue) {
        update(state => ({
            ...state,
            data: state.data.map(x => x.id === id ? newEntry : x)
        }))
    },
    requestFailed: (error) => {
      set({
        status: "failed",
        error,
        data: null,
      });
    },
    issues: derived({ subscribe }, ($state) => $state.data),
  };
}

export const issuesStore = createIssuesStore();
