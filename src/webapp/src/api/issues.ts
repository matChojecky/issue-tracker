import type { Issue, IssueDTO } from "../../../domain/issue/model";

export async function getIssues(): Promise<Issue[]> {
    const res = await fetch('/api/issues');
    const data = await res.json();
    return data;
} 

export async function addIssue(issue: IssueDTO): Promise<Issue> {
    const res = await fetch('/api/issues', {
        body: JSON.stringify(issue),
        method: "post",
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const data = await res.json();
    return data;
}

export async function removeIssue(id: string): Promise<void> {
     await fetch(`/api/issues/${id}`, {
        method: "delete"
    });
}

export async function updateIssue(id: string, updates: Partial<IssueDTO>) {
    const res = await fetch(`/api/issues/${id}`, {
        body: JSON.stringify(updates),
        method: "put",
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const data = await res.json();
    return data;

} 