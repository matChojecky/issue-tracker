import { IssueStatus } from "../../../domain/issue/model";

const statusStrings = {
  [IssueStatus.Open]: "Opened",
  [IssueStatus.Pending]: "Pending",
  [IssueStatus.Closed]: "Closed",
};
export const getIssueStatusText = (status: IssueStatus) => {
  return statusStrings[status];
};

export const getStatusSelectOptions = (): {value: IssueStatus, label: string}[] => {
  return Object.keys(statusStrings).map((key) => ({
    value: Number(key),
    label: statusStrings[key],
  }));
};
