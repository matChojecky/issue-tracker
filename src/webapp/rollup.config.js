import svelte from "rollup-plugin-svelte";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import livereload from "rollup-plugin-livereload";
import { terser } from "rollup-plugin-terser";
import sveltePreprocess from "svelte-preprocess";
import typescript from "@rollup/plugin-typescript";
import copy from 'rollup-plugin-copy'
import path from "path";

const production = !process.env.ROLLUP_WATCH;

export default {
  input: path.resolve(__dirname, "src/main.ts"),
  output: {
    sourcemap: true,
    format: "iife",
    name: "app",
    file: path.resolve(__dirname, "public/build/bundle.js"),
  },
  plugins: [
    svelte({
      // enable run-time checks when not in production
      dev: !production,
      // we'll extract any component CSS out into
      // a separate file - better for performance
      css: (css) => {
        css.write(path.resolve(__dirname, "public/build/bundle.css"));
      },
      preprocess: sveltePreprocess(),
    }),

    // If you have external dependencies installed from
    // npm, you'll most likely need these plugins. In
    // some cases you'll need additional configuration -
    // consult the documentation for details:
    // https://github.com/rollup/plugins/tree/master/packages/commonjs
    resolve({
      browser: true,
      dedupe: ["svelte"],
    }),
    commonjs(),
    typescript({
      sourceMap: true,
      tsconfig: path.resolve(__dirname, "tsconfig.json"),
    }),

    // Watch the `public` directory and refresh the
    // browser on changes when not in production
    !production && livereload("public"),

    // If we're building for production (npm run build
    // instead of npm run dev), minify
    production && terser(),

    production && sleeper("generateBundle", 5000),

    copy({ 
      targets: [{
        src: path.resolve(__dirname, './public').split(path.sep).join(path.posix.sep),
        dest: "dist/webapp"
      }],
      verbose: true,
      hook: "writeBundle"
    }),
  ],
  watch: {
    clearScreen: false,
  },
};

async function sleep(ms) {
  return new Promise(res => {
    setTimeout(res, ms)
  })
}

function sleeper(hook, ms) {
  return {
    name: 'sleeper',
    [hook]: async () => {
      await sleep(ms);
    }
  }
}
