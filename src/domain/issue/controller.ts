import {Router} from 'express';
import * as IssueService from './service';
import Status from 'http-status';


const router = Router();



router.get('/', (_, res) => {
    res.json(IssueService.getAll());
});

router.get('/:id', (req, res) => {
    const issue = IssueService.getOne(req.params.id);
    if(!issue) {
        res.status(Status.NO_CONTENT).send(null);
    }

    res.status(Status.OK).json(issue);
});


router.post('/', (req, res) => {
    try {
        const new_issue = IssueService.add(req.body);
        res.status(Status.OK).json(new_issue);
    } catch (err) {
        res.status(Status.INTERNAL_SERVER_ERROR).json(err);
    }
});

router.put('/:id', (req, res) => {
    try {
        const updated = IssueService.update(req.params.id, req.body);
        res.status(Status.OK).json(updated);
    } catch (err) {
        res.status(Status.INTERNAL_SERVER_ERROR).json(err);
    }
});

router.delete('/:id', (req, res) => {
    IssueService.remove(req.params.id);
    res.status(Status.OK).send();
});

export const registerIssuesController = (api: Router) => {
    api.use('/issues', router)
};