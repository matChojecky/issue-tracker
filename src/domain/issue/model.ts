export interface Issue {
    id: string;
    title: string;
    description: string;
    status: IssueStatus
}

export enum IssueStatus {
    Open,
    Pending,
    Closed
}


export interface IssueDTO {
    title: string;
    description: string;
    status: IssueStatus
}