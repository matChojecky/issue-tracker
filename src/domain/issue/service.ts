import { v4 as uuidv4 } from 'uuid';
import { Issue, IssueDTO, IssueStatus } from "./model";
const faker = require('faker');

const datasource: {[id: string]: Issue} = {};

function getRandomStatus() {
    return [IssueStatus.Open, IssueStatus.Pending, IssueStatus.Closed][Math.floor(Math.random() * 3)]
}


for(let i = 0; i < 10; i++) {
    const id = uuidv4();
    datasource[id] = {
        id,
        title: faker.fake("{{name.firstName}} {{hacker.phrase}} {{random.words}}"),
        description: faker.fake("{{hacker.phrase}} {{random.words}} {{hacker.phrase}} {{random.words}} {{hacker.phrase}} {{random.words}} {{hacker.phrase}} {{random.words}}"),
        status: getRandomStatus()
    }; 
}

export function getAll(): Issue[] {
    return Object.values(datasource);
}

export function getOne(id: string): Issue | null {
    const entity = datasource[id];
    if(!entity) {
        return null;
    }
    return entity;
}

export function update(id: string, updates: Partial<IssueDTO>): Issue {
    const entity = datasource[id];

    if(!entity) {
        throw new Error('Issue does not exist!');
    }

    if(!!updates.status && (!(updates.status in IssueStatus) || updates.status <= entity.status)) {
        delete updates.status;
    }

    datasource[id] = {
        ...entity,
        ...updates
    }

    return datasource[id];
}

export function add(issue: IssueDTO): Issue {
    const id = uuidv4();
    const newIssue: Issue = {
        ...issue,
        id,
        status: issue.status in IssueStatus ? issue.status : IssueStatus.Open,
    }

    datasource[id] = newIssue;

    return newIssue;
}

export function remove(id: string): void {
    delete datasource[id];
} 

