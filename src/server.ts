import path from "path";
import express, { Application } from "express";
import bodyParser from 'body-parser';
import compression from 'compression';
import { registerApiRouter } from "./infrastructure/http/api-router";

const app: Application = express();

app.use(bodyParser.json());
app.use(compression());

app.use(express.static(path.join(__dirname, "./webapp/public")));

registerApiRouter(app);

app.listen(process.env.PORT || 3000, () => {
  console.log("listening on port: ", process.env.PORT || 3000);
});
